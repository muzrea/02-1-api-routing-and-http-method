package com.twuc.webApp.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class StudentController {
    @GetMapping("api/employee")
    ResponseEntity<String> getStu(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("x-id","1");
        return ResponseEntity.ok()
                .header("x-id", "1")
                .body("getStu");
    }

    @PostMapping("/api/employee")
    ResponseEntity<Employee> getStudent(@RequestBody Employee employee){
        return ResponseEntity.ok()
                .body(employee);
    }

}
