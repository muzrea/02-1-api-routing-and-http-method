package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class UserBookController {
    @GetMapping("/users/{userId}/books")
    public String getUserBook(@PathVariable("userId") Long userId) {
        return "The book for user " + userId;
    }

    @GetMapping("/users/books/{id}")
    public String getUserBookUppercase(@PathVariable("ID") Long ID) {
        return "The book for user " + ID;
    }

    @GetMapping("/segments/good")
    public String getGood() {
        return "This is a segment";
    }

    @GetMapping("/segments/{segmentName}")
    public String getSegmentName(@PathVariable("segmentName") String segmentName) {
        return "This is " + segmentName;
    }

    @GetMapping("/students")
    public String getStudents(@RequestParam String gender, @RequestParam(name = "class") String clazz) {
        return gender + clazz;
    }

    @PostMapping("/student")
    public String createStudent(@RequestBody @Valid Student student) {
        return student.getName();
    }

    @GetMapping("/user/?/book")
    public String getBooks() {
        return "getBooks";
    }

    @GetMapping("/user/book/?")
    public String getBooksByEnd() {
        return "getBooksByEnd";
    }

    @GetMapping("/wildcards/*")
    public String verifyWildcards() {
        return "verifyWildcards";
    }

    @GetMapping("/wildcards/before/*/after")
    public String verifyWildcardsInMiddle() {
        return "verifyWildcardsInMiddle";
    }

    @GetMapping("/wildcards/use/*after*")
    public String verifyWildcardsAsPrefixAndPostfix() {
        return "verifyWildcardsAsPrefixAndPostfix";
    }

    @GetMapping("/wildcards/segment/**/after")
    public String verifyWildcardsInEnd() {
        return "verifyWildcardsInEnd";
    }

    @GetMapping("/wildcards/segment/{[abc]}")
    public String verifyRegex() {
        return "verifyRegex";
    }
}
