package com.twuc.webApp.controller;

public class Employee {
    private String name;

    private Integer age;
    private Integer phoneNumber;

    public Employee(){

    }
    public Employee(String name,Integer age,Integer phontNumber){
        this.name = name;
        this.age = age;
        this.phoneNumber = phontNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
