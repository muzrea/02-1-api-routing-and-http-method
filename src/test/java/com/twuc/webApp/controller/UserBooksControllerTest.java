package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserBooksControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_userId() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(content().string("The book for user 2"))
                .andExpect(status().isOk())
        ;
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(content().string("The book for user 23"))
                .andExpect(status().isOk())
        ;
    }

    @Test
    void should_return_false_in_different_case() throws Exception {
        mockMvc.perform(get("/api/users/books/6"))
                .andExpect(status().is(500));
    }

    @Test
    void should_call_exact_api() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(content().string("This is a segment"))
                .andExpect(status().isOk());
    }

    @Test
    void should_create_student_and_return_name() throws Exception {
        mockMvc.perform(post("/api/student").content("{\"name\":\"xiaoli\",\"gender\":\"female\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("xiaoli"))
                .andExpect(status().isOk());
    }

    @Test
    void should_create_student_if_name_is_null_return_bad_request() throws Exception {
        mockMvc.perform(post("/api/student").content("{\"gender\":\"female\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_404_when_routing_greater_than_1() throws Exception {
        mockMvc.perform(get("/api/user/aaa/book"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_404_when_routing_is_null() throws Exception {
        mockMvc.perform(get("/api/user//book"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_404_when_at_the_end_() throws Exception {
        mockMvc.perform(get("/api/user/book/a"))
                .andExpect(status().isOk())
                .andExpect(content().string("getBooksByEnd"));
    }

    @Test
    void should_match_segment_when_use_multiply_words_wildcards() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything"))
                .andExpect(status().isOk())
                .andExpect(content().string("verifyWildcards"));

    }

    @Test
    void should_return_verify_wildcards_in_middle_when_use_multiply_words_wildcards_in_middle() throws Exception {
        mockMvc.perform(get("/api/wildcards/before/anything/after"))
                .andExpect(status().isOk())
                .andExpect(content().string("verifyWildcardsInMiddle"));
    }

    @Test
    void should_return_404_when_use_multiply__wildcards_in_middle_with_no_segment() throws Exception {
        mockMvc.perform(get("/api/wildcards/before//after"))
                .andExpect(status().is(404));
    }

    @Test
    void should_match_when_use_segment_words_wildcards_in_start_and_end() throws Exception {
        mockMvc.perform(get("/api/wildcards/use/aafteraaa"))
                .andExpect(status().isOk())
                .andExpect(content().string("verifyWildcardsAsPrefixAndPostfix"));
    }

    @Test
    void should_match_when_use_multiply_words_wildcards_in_middle() throws Exception {
        mockMvc.perform(get("/api/wildcards/segment/anything/after"))
                .andExpect(status().isOk())
                .andExpect(content().string("verifyWildcardsInEnd"));
    }

    @Test
    void should_not_match_when_use_regex() throws Exception {
        mockMvc.perform(get("/api/wildcards/segment/a"))
                .andExpect(status().isOk())
                .andExpect(content().string("verifyRegex"));
    }

    @Test
    void should_return_gender_and_class() throws Exception {
        mockMvc.perform(get("/api/students?gender=female&class=A"))
                .andExpect(content().string("femaleA"))
                .andExpect(status().isOk());
    }

    //声明了@RequestParam 的参数但是访问url时没提供返回400
    @Test
    void should_return_gender() throws Exception {
        mockMvc.perform(get("/api/students?gender=female"))
                .andExpect(status().is(400));
    }
}
