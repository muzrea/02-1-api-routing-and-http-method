package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class SerializeTest {
    @Autowired
    MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void getObjectMapper() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void should_serialize_successfully() throws IOException {
        String json = "1";
        objectMapper = new ObjectMapper();
        Integer num =  objectMapper.readValue(json,Integer.class);
        assertEquals(num,json);
    }
}
