package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_responseEntity_with_custom_status_header_content_content_type() throws Exception {
        mockMvc.perform(get("/api/employee"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("getStu"));
//                .andExpect(header().string("xml-id"));
        ;
    }

    @Test
    void should_return_student_with_custom_status_header_content_content_type() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(post("/api/employee")
                .contentType("application/json;charset=UTF-8")
                .content("{\"name\":\"kamil\",\"age\":19,\"phoneNumber\":1111}"))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"name\":\"kamil\",\"age\":19,\"phoneNumber\":1111}"));

    }

    @Test
    void should_return_student_with_name_and_phoneNumber() throws Exception {
        mockMvc.perform(post("/api/employee")
                .contentType("application/json;charset=UTF-8")
                .content("{\"name\":\"kamil\",\"phoneNumber\":1111}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("{\"name\":\"kamil\",\"phoneNumber\":1111}"));
        ;

    }
}
